variable "description" {
  description = "Description of what your Lambda Function does."
}

variable "lambda_zipfile" {
  description = "The path to the function's deployment package within the local filesystem."
}

variable "function_name" {
  description = "Overrides the default. A unique name for your Lambda Function."
  default     = ""
}

variable "runtime" {
  description = "See [Runtimes](https://docs.aws.amazon.com/lambda/latest/dg/API_CreateFunction.html#SSS-CreateFunction-request-Runtime) for valid values."
  default     = "nodejs4.3"
}

variable "timeout" {
  description = "The amount of time your Lambda Function has to run in seconds. Defaults to 3. See [Limits](https://docs.aws.amazon.com/lambda/latest/dg/limits.html)"
  default     = "3"
}

variable "memory_size" {
  description = "The amount of memory your Lambda Function has to run on. Defaults to 128. See [Limits](https://docs.aws.amazon.com/lambda/latest/dg/limits.html)"
  default     = "128"
}

variable "handler" {
  description = "The function entrypoint in your code."
}

variable "env_vars" {
  description = "A map of the environment variables you wish to be on the function."
  type        = map(string)
  default     = {}
}

variable "account_id" {
  default = "823193265824"
}

variable "name_override" {
  default = ""
}

variable "app" {
  description = "The Tag Project"
}

variable "service" {
  description = "service"
}

variable "region" {
  default = "us-east-1"
}

variable "ship_logs" {
  default = false
}

variable "log_function_name" {
  default = ""
}

variable "env" {
  description = "environment"
  type        = string
}

variable "enable_vpc" {
  default     = false
  description = "This is no longer needed. It does nothing."
}

variable "subnet_ids" {
  description = "A list of subnet IDs associated with the Lambda function."
  type        = list(string)
  default     = []
}

variable "security_group_ids" {
  description = "A list of security group IDs associated with the Lambda function."
  type        = list(string)
  default     = []
}

variable "tracing_mode" {
  default = "PassThrough"
}

variable "extra_tags" {
  default = {}
}

variable "log_retention" {
  description = "How many days do you want to keep the logs?"
  default     = 30
}

variable "extra_role_principals" {
  type    = list(string)
  default = []
}


variable "scheduled_events" {
  description = <<EOF
  Expected structure:

  type = list(object({
    schedule_expression = string
    data = object
  }))
EOF
  type = list(object({
    schedule_expression = string
    data                = map(any)
  }))
  default = []
}

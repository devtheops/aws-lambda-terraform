### Description

This module creates a lambda resource, assume role policy, vpc policy and a role to attach the policies and the lambda too.

### Variables

| **Name** | **Type** | **Description** | **Default** |
| -------- | ---- | ----------- | ------- |
| description | `unknown` | Description of what your Lambda Function does. | *None* |
| lambda_zipfile | `unknown` | The path to the function's deployment package within the local filesystem. | *None* |
| functionname | `unknown` | A unique name for your Lambda Function. | *None* |
| runtimelanguage | `unknown` | See [Runtimes](https://docs.aws.amazon.com/lambda/latest/dg/API_CreateFunction.html#SSS-CreateFunction-request-Runtime) for valid values. | `"nodejs4.3"` |
| timeout | `unknown` | The amount of time your Lambda Function has to run in seconds. Defaults to 3. See [Limits](https://docs.aws.amazon.com/lambda/latest/dg/limits.html) | `"3"` |
| memory_size | `unknown` | The amount of memory your Lambda Function has to run on. Defaults to 128. See [Limits](https://docs.aws.amazon.com/lambda/latest/dg/limits.html) | `"128"` |
| handler | `unknown` | The function entrypoint in your code. | *None* |
| env_vars | `map` | A map of the environment variables you wish to be on the function. | `{"ATLEASTONE": "HASTOBEHERE"}` |
| app | `unknown` | The Tag Project | *None* |
| enable_vpc | `bool` | No description provided. Please add a description to this variable. | `false` |
| subnet_ids | `list` | A list of subnet IDs associated with the Lambda function. | `[]` |
| security_group_ids | `list` | A list of security group IDs associated with the Lambda function. | `[]` |
| tracing_mode | `unknown` | No description provided. Please add a description to this variable. | `"PassThrough"` |

### Outputs

| **Name** | **Description** |
| -------- | --------------- |
| function_arn | No description provided. Please add a description to this variable. |
| function_name | No description provided. Please add a description to this variable. |
| function_invoke_arn | No description provided. Please add a description to this variable. |
| role_arn | No description provided. Please add a description to this variable. |
| role_name | No description provided. Please add a description to this variable. |

### Resources used

* aws_lambda_function
* aws_iam_role
* aws_iam_policy
* aws_iam_policy_attachment
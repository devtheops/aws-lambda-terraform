locals {

  log_function_name = length(var.log_function_name) > 0 ? var.log_function_name : "cw-to-logzio-${var.env}"

  function_name_default = "${var.app}-${var.service}-${var.env}"
  function_name         = length(var.function_name) > 0 ? var.function_name : local.function_name_default

  tags_default = {
    app       = var.app
    service   = var.service
    env       = var.env
    terraform = "true"
  }
  tags = merge(local.tags_default, var.extra_tags)

  vpc_config_on = {
    subnet_ids         = var.subnet_ids
    security_group_ids = var.security_group_ids
  }
  vpc_config = length(var.security_group_ids) > 0 ? list(local.vpc_config_on) : []


  env_vars_on = {
    variables = var.env_vars
  }
  env_vars = length(var.env_vars) > 0 ? list(local.env_vars_on) : []

  # This is for resources that have length limits and support name_prefix
  name_prefix = length(local.function_name) > 50 ? "${var.app}-${var.service}" : null

}

locals {
  base_principals = ["lambda.amazonaws.com"]
  principals      = distinct(concat(local.base_principals, var.extra_role_principals))
}

data "template_file" "lambda_assume_policy" {
  template = file("${path.module}/files/assume-policy.json")

  vars = {
    principals = jsonencode(local.principals)
  }
}

data "template_file" "lambda_base_policy" {
  template = file("${path.module}/files/base-policy.json")
}

data "template_file" "lambda_vpc_policy" {
  template = file("${path.module}/files/vpc-policy.json")
}

data "template_file" "lambda_xray_policy" {
  template = file("${path.module}/files/xray-policy.json")
}

resource "aws_iam_role" "main" {
  name               = local.name_prefix == null ? "${local.function_name}-lambda" : null
  name_prefix        = local.name_prefix
  description        = "IAM role for ${local.function_name}"
  assume_role_policy = data.template_file.lambda_assume_policy.rendered
  tags               = local.tags
}


resource "aws_iam_policy" "base-policy" {
  name        = local.name_prefix == null ? "${local.function_name}-lambda-base" : null
  name_prefix = local.name_prefix
  description = "base Policy"
  policy      = data.template_file.lambda_base_policy.rendered
}

resource "aws_iam_policy_attachment" "base-attach" {
  name       = "${local.function_name}-base"
  roles      = [aws_iam_role.main.name]
  policy_arn = join("", aws_iam_policy.base-policy.*.arn)
}


resource "aws_iam_policy" "vpc-policy" {
  count       = length(var.security_group_ids) > 0 ? 1 : 0
  name        = local.name_prefix == null ? "${local.function_name}-lambda-vpc" : null
  name_prefix = local.name_prefix
  description = "VPC Policy"
  policy      = data.template_file.lambda_vpc_policy.rendered
}

resource "aws_iam_policy_attachment" "vpc-attach" {
  count      = length(var.security_group_ids) > 0 ? 1 : 0
  name       = "${local.function_name}-vpc"
  roles      = [aws_iam_role.main.name]
  policy_arn = join("", aws_iam_policy.vpc-policy.*.arn)
}


resource "aws_iam_policy" "xray-policy" {
  count       = var.tracing_mode == "Active" ? 1 : 0
  name        = local.name_prefix == null ? "${local.function_name}-lambda-xray" : null
  name_prefix = local.name_prefix
  description = "xray Policy"
  policy      = data.template_file.lambda_xray_policy.rendered
}

resource "aws_iam_policy_attachment" "xray-attach" {
  count      = var.tracing_mode == "Active" ? 1 : 0
  name       = "${local.function_name}-xray"
  roles      = [aws_iam_role.main.name]
  policy_arn = join("", aws_iam_policy.xray-policy.*.arn)
}

